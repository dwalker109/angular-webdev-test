import './contact-form.style.scss';

export default {
  template:
`
<form name="contactForm" novalidate>
  <h1>Add/Edit Contacts</h1>
  <label for="fname">First name</label>
  <input type="text" name="fname" id="fname" ng-model="$ctrl.model.fname" required>
  <span ng-show="contactForm.fname.$invalid">Please enter your first name</span>
  <br>

  <label for="lname">Last name</label>
  <input type="text" name="lname" id="lname" ng-model="$ctrl.model.lname" required>
  <span ng-show="contactForm.lname.$invalid">Please enter your last name</span>
  <br>

  <label for="email">Email address</label>
  <input type="email" name="email" id="email" ng-model="$ctrl.model.email" required>
  <span ng-show="contactForm.email.$invalid">Please enter a valid email address</span>
  <br>

  <label for="company">Company</label>
  <input type="text" name="company" id="company" ng-model="$ctrl.model.company"><br>

  <label for="job_title">Job title</label>
  <input type="text" name="job_title" id="job_title" ng-model="$ctrl.model.job_title"><br>

  <label for="phone">Phone number</label>
  <input type="text" name="phone" id="phone" ng-model="$ctrl.model.phone"><br>

  <label for="birthday">Birthday</label>
  <input type="date" name="birthday" id="birthday" ng-model="$ctrl.model.birthday">
  <span ng-show="contactForm.birthday.$invalid">Please enter a valid birthday</span>
  <br>

  <label for="address">Address</label>
  <textarea name="address" id="address" ng-model="$ctrl.model.address"></textarea><br>

  <label for="notes">Notes</label>
  <textarea name="notes" id="notes" ng-model="$ctrl.model.notes"></textarea><br>

  <button ng-disabled="contactForm.$invalid" type="submit" ng-click="onClickedSubmit($ctrl.model)">
    {{ $ctrl.model._id ? 'Save' : 'Add' }}
  </button>
</form>
`,
  controller: ($scope, $window, ContactsService) => {
    $scope.contacts = ContactsService;
    $scope.$ctrl.model = $scope.contacts.current;
    
    $scope.onClickedSubmit = (model) => {
      if ($scope.contactForm.$valid) {
        ContactsService.save(model);
      }
    };
  }
};
