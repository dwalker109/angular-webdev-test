export default ($http) => {
  let list = [];
  let current = {};
   
  let refresh = () => {
    $http.get('/api/contacts').then((response) => {
      angular.copy(response.data, list)
    });
  };
   
  return {
    list: list,
    current: current,
    init: () => {
      refresh();
    },
    save: (model) => {
      $http.post('/api/contacts', model).then((response) => {
        angular.copy({}, current)
        refresh();
      });
    },
    load: (model) => {
      angular.copy(model, current)
    },
    delete: (model) => {
      $http.delete('/api/contacts', model).then((response) => {
        refresh();
      });
    }
  };
};
