import './contacts-list.style.scss';

export default {
  template:
`
<div class="contacts-list">
  <h1>Existing Contacts</h4>
  <div ng-if="!contacts.list.length">
    <p>You have no contacts.</p>
  </div>
  <div ng-if="contacts.list.length">
    <button ng-click="exportLink()">Export all contacts</button>
    <input class="contact-search" ng-model="search" type="text" placeholder="Type to search">
    <ul>
      <li class="contact-item" ng-repeat="contact in contacts.list | filter:search">
        {{ contact.fname }} {{ contact.lname }} ({{ contact.email }})
        <button ng-click="contacts.load(contact)">Edit</button>
        <button ng-click="contacts.delete(contact)">Delete</button>
      </li>
    </ul>
  </div>
</div>
`,
  controller: ($scope, ContactsService) => {
    $scope.contacts = ContactsService;
    $scope.contacts.init();
    
    $scope.exportLink = () => {
      let data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify($scope.contacts.list));
      let el = document.createElement('a');

      el.setAttribute('href', 'data:' + data);
      el.setAttribute('download', 'export.json');
      el.click();
    };
  }
};
